# import libraries
import folium
import utm
import pandas as pd
from numpy import random
from geopy.distance import geodesic


#FF0000	Red
#FFFF00	
#FFFF00	Yellow
#7FFF00	
#00FF00	Green
colors = ['#FF0000','#FFFF00','#FFFF00','#7FFF00','#00FF00']

############################################################
##Function definitions
############################################################

def getTimeToSec(time):
    #string[ start_index_pos: end_index_pos: step_size]
    time_int = int(time,16)
    time_str = "%.8d" % (int(time,16))
    hours = int(time_str[0:2:1],10)
    minutes = int(time_str[2:4:1],10)
    seconds = int(time_str[4:6:1],10)
    milliseconds = int(time_str[6:8:1],10)
    return (hours*3600)+(minutes*60)+seconds + int(milliseconds/1000)
    
    

def getSpeed(coord1, coord2, time1, time2):
    distance = geodesic(coord1, coord2).m
    if ((time2 - time1)> 0):
        speed = distance / (time2 - time1)
    else:
        speed = 0;
    return round(speed, 2)


def getSpeedColor(speed):
    if(speed < 2):
            return colors[4]
    elif(speed < 4):
            return colors[3]
    elif(speed < 6):
            return colors[2]
    elif(speed < 8):
            return colors[1]        
    elif(speed < 10):           #36 km/h
            return colors[0]    


        
def setMapSpeedPath(listLatLon,gpsStamp):
    folium.Marker(location=listLatLon[0],tooltip='Start').add_to(m)
    folium.Marker(location=listLatLon[len(listLatLon)-1],tooltip='Stop').add_to(m) #Last coordinate of the list
    
    for x in range(0,len(listLatLon)-30,30):
        time1 = getTimeToSec(gpsStamp[x])
        time2 = getTimeToSec(gpsStamp[x+30])    
        speed = getSpeed(listLatLon[x],listLatLon[x+30],time1,time2)
        folium.vector_layers.PolyLine(listLatLon[x:x+30:1],popup='<b>Path Vehicle</b>', tooltip= str(speed) + " m/s", color = getSpeedColor(speed), weight=5, opacity= 0.5).add_to(m)
    m.save('indexSpeedPath.html')

   

   

def setMapMarkers(listLatLon,gpsStamp):
    for i in range(0, len(listLatLon),300):                                           # The third argument jumps the number of elements to print, this way we can reduce the number of printed markers
        folium.Marker(location= [listLatLon[i][0],listLatLon[i][1]], popup=str(int(gpsStamp[i],16))+' s').add_to(m)
    m.save('indexMarkers.html')



 
def getListConversionToLatLon(utm_x,utm_y,zone):
    temp_latlon = []
    for i in range(0, len(utm_x),1):    #verificare per quale lunghezza               
        temp_latlon.append(utm.to_latlon(int(utm_x[i], 16),int(utm_y[i], 16), int(zone[i], 16), 'northern'))   
    return temp_latlon

    


 
#Open document and conversion, lines starting with '#' are considered as comments
data = pd.read_csv('new files.txt', sep=",", header=None, comment = '#')
data.columns = ["Number", "Time", "Identifier", "Length", "Data"]

#Filter data by ID
data_utm      = data[data['Identifier']=='000007F1'] # UMT_x and UTM_y
data_gpsStamp = data[data['Identifier']=='000007F2'] # GPS time stamp
data_zone     = data[data['Identifier']=='000007F3'] # from 1 to 60 zones

#Coordinates repartition
utm_y = data_utm.Data.str.slice(start=8)
utm_x = data_utm.Data.str.slice(start=0,stop=8)
zone  = data_zone.Data.str.slice(start=0)
gpsStamp = data_gpsStamp.Data

#Reset the index, ex: 1,2,4,6 -> 0,1,2,3
utm_x = utm_x.reset_index(drop=True)
utm_y = utm_y.reset_index(drop=True)
zone = zone.reset_index(drop=True)
gpsStamp = gpsStamp.reset_index(drop=True)

utm_y_mean = 0
utm_x_mean = 0
zone_mean = 0
for x in range(0,len(data_utm)):
    utm_x_mean = utm_x_mean + int(utm_x[x], 16)
    utm_y_mean = utm_y_mean + int(utm_y[x], 16)

for i in range(0,len(data_zone)):    
    zone_mean = zone_mean + int(zone[i], 16)

utm_x_temp = utm_x_mean / len(data_utm)
utm_y_temp = utm_y_mean / len(data_utm)
zone_mean  = zone_mean / len(data_zone)

temp_mean = utm.to_latlon(utm_x_temp,utm_y_temp, zone_mean, 'northern')
m = folium.Map(location=[temp_mean[0],temp_mean[1]],zoom_start=13)              #zoom_start max = 18

listLatLon = getListConversionToLatLon(utm_x,utm_y,zone)

##setMapSpeedPath(listLatLon,gpsStamp)
setMapMarkers(listLatLon,gpsStamp)









    
